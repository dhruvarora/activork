from django import template
import re
register = template.Library()

@register.filter(name='split')
def split(value, arg):
    value = [x for x in value.split(",") if re.search(r'[a-zA-Z0-9]+',x)]
    return value



@register.filter(name='get_url')
def get_url(value):
	id = value.split("/")[-1]
	return "https://www.youtube.com/embed/"+str(id)


@register.filter(name='get_source')
def get_source(value):
	#return "abcde"
	li = ["www.","in."]

	try:
		value = value.split("//")[1].split("/")[0]
		for i in li:
			if i in value:
				return value.split(i)[1].split(".")[0]
		else:
			return value
	except:
		return value
	#return value.split("//")[1].split("/")[0]

@register.filter(name='check_string')
def check_string(value):
    check = "articles"
    if check in value:
        return True
    else:
        return False

@register.filter(name='check_event_string')
def check_string(value):
    check = "events"
    if check in value:
        return True
    else:
        return False
