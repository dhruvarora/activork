$('#auth_form').on('submit', function(event) {
  var error = false;
  $('#login_btn').prop('disabled', true);
  var email = $("input[name=email]").val();
  var password = $("input[name=password]").val();
  console.log("email : " + email + " /\ password : " + password);
  if (!email || !password) {
	//alert("nn");

    event.preventDefault();
    var offset = $("#login_form").offset();
    console.log("offset = " + offset.top);
    $("#login_form").animate({top:"70px"}, 500, function() {console.log("sucess");});
    $('.login-error').css('display', 'block');
    $('.login-error').addClass('animated fadeInLeft');
    $('#login_btn').prop('disabled', false);
  } else {
    var csrf_token = $('input[name=csrfmiddlewaretoken]').val();
    var rel = "";
    $.ajax({
      url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/login/',
      data: {token:csrf_token, email:email, password:password},
      async: false,
      type: 'POST',
      success: function(data) {
        rel = data
     	//alert(data["error"]) ;
        console.log("Data : " + data);
        if (data["error"]) {
	  
          event.preventDefault(); // Do not submit the form
          error = true;
	 
          $('.login-error').css('display', 'block');
          $('.login-error').addClass('animated fadeInLeft');
          $('.login-error').html(data.error);
          $('#login_btn').prop('disabled', false);
        }
      }
    });
    if (!error) {
      console.log("Rel : " + rel.SESSION_ID);
      Cookies.set('user_session_cookie', rel.SESSION_ID, {expires:1});
      Cookies.set('email', rel.user_data.email, {expires:1});
      Cookies.set('first_name', rel.user_data.first_name, {expires:1});
      Cookies.set('last_name', rel.user_data.last_name, {expires:1});
      Cookies.set('phone', rel.user_data.phone_no, {expires:1});
      Cookies.set('username', rel.user_data.username, {expires:1});
      Cookies.set('profile_image',rel.profile_image,{expires:1});
    }
  }

});

$('#register').on('click', function(event) {
  //event.preventDefault();
  // Disable the submit button
  $('#submit').prop('disabled', true);
  $('#submit').html('Registering');

  var username = $('input[name=username]').val();
  var email = $('input[name="email"]').val();
  var password = $('input[name="password"]').val();
  var phone = $('input[name="phone"]').val();

	var noError = false;

  console.log(username);

  if (!username || !email || !password || !phone) {
    console.log('error');
    $("#status_text").addClass('error');
    $('#submit').prop('disabled', false);
    $('#submit').html('Register');
  } else {
    var csrf_token = $('input[name=csrfmiddlewaretoken]').val();
    console.log(csrf_token);
    $.ajax({
      url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/signup/',
      data: {csrfmiddlewaretoken:csrf_token, username:username, email:email, phone_no:phone, password:password},
      type: 'POST',
      async:false,
      success: function(data) {

	//alert(JSON.stringify(data));
        if (data["error"]) {
	//alert("error");
          $('#status_text').html(data.error);
          $("#status_text").addClass('error');
          $('#submit').prop('disabled', false);
          $('#submit').html('Register');
	event.preventDefault();
	return false;
        } else {
	  noError = true;
		//return true;
		$("#auth_reg_form").submit();
	
        }
      },
      error: function(error) {
        console.log(error);
      }
    });
	//return false;
  }
//return true;
//event.preventDefault();

});

$('#forgot_password').on('click', function() {
  var email = $('input[name="email"]');
  if (email.val() == '' || email.val() == null) {
    email.addClass('error-border');
  } else {
    $.ajax({
      url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/forgot_password/',
      type: 'POST',
      data: {
        email: email.val()
      },
      success: function(res) {
        console.log(res)
      }
    })
  }
})
