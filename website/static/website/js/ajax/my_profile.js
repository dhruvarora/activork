var session_id = Cookies.get('user_session_cookie');
var my_article_data = {};

function my_profile_detail() {
  console.log(session_id);
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/myprofile_userdetails/',
    type: 'GET',
    beforeSend: function (request)
            {
                request.setRequestHeader("SESSION-ID", session_id);
            },
    success: function(data) {
      console.log(data);
      if (data['data']['user'] == Cookies.get('username')) {
        var edit_html = '';
        edit_html += '<div class="reward-btn-parent" style="padding-bottom:2em;">';
        edit_html += '    <a href="#" class="reward-btn" style="margin-right:0.5em;" id="edit_profile">Edit</a>';
        edit_html += '    <a href="#status_form" rel="modal:open" class="reward-btn" id="change_status">Change Status</a>';
        edit_html += '</div>';
        $('#my_detail').append(edit_html);

        $('#edit_profile').on('click', function() {
          console.log('edit profile clicked');
        });
      }

    },
    error: function(error) {
      console.log(error);
    }
  });
}

function my_profile_status() {
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/get_status/?user=self',
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("SESSION-ID", session_id);
    },
    success: function(data) {
      console.log(data);
    }
  });
}

function my_articles() {
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/myprofile_articles/?filter=latest',
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("SESSION-ID", session_id);
    },
    success: function(data) {
      my_article_data = data;
      console.log('My Articles:');
      console.log(my_article_data);

      if (my_article_data['status'] == 0)
        $('#my_articles').html('<strong>' + my_article_data['error'] + '</strong>');
      else {

      $.each(my_article_data['data'], function(i, val) {
        article_data = my_article_data['data'];

        tags_arr = article_data[i]['tags'].split(',');

        var div = '';
        div += '<div id="card{{forloop.counter0}}" class="card visible-sm visible-md visible-lg {% if forloop.counter0 > 4 %}hide-card{% endif %}" style="width:100%;margin:0 auto;margin-bottom:10px;">';
        div += '    <div class="row row-eq-height" style="padding: 1em 0.5em;">';
        div += '        <div class="col-xs-12 col-sm-12 col-md-3">';
        div += '            <a href="/website/articles/'+ article_data[i]['id'] +'"><div class="img" style="background-image:url(\'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com'+ article_data[i]['image'] +'\')"></div></a>';
        div += '        </div>';
        div += '        <div class="col-xs-12 col-sm-12 col-md-9">';
        div += '          <h4 class="card-title">';
        div += '            <a href="/website/articles/'+ article_data[i]['id'] +'">'+ article_data[i]['name'] +'</a>';
        div += '            <a href="#" id="bookmark_article_{{article_detail.id}}" rel="{{article_detail.id}}" style="position:absolute;right:15px;"><img id="bookmark-img-{{article_detail.id}}" src="" width="25" alt="" /></a>';
        div += '          </h4>';
        div += '          <p class="card-text">';
        div += '            <span>'+ article_data[i]['source'] +'</span>';
        div += '            <div class="tag-list">';
        if (tags_arr.length > 1) {
          console.log('Tags length : ' + tags_arr.length);
          div += '              <ul class="list-inline">';
          $.each(tags_arr, function(i, val) {
            div += '<li><a href="#">'+ val +'</a></li>'
          });
          div += '              </ul>';
        }
        //div += '                  <li><a href="#">{{tag}}</a></li>';
        div += '            </div></p>';
        div += '          <div class="hidden-xs hidden-sm" style="position:absolute;bottom:0;">';
        div += '                              <a href="#" class="card-link" id="like_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/like.png" %}" width="20"  /> 0</a>';
        div += '              <a href="#" class="card-link"><img src="/static/website/images/icons/share.png"  width="20" /> 10</a>';
        div += '                              <a href="#" class="card-link" id="comment_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/comments.png" width="20"  /> 0</a>';

        div += '          </div>';
        div += '        </div>';
        div += '        <div class="col-xs-12 col-sm-12 visible-sm" style="margin-top:15px;">';
        div += '          <a href="#" class="card-link" id="like_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/like.png" width="20" /> 0</a>';
        div += '          <a href="#" class="card-link"><img src="/static/website/images/icons/share.png" width="20" /> 10</a>';
        div += '          <a href="#" class="card-link" id="m_comment_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/comments.png" width="20" /> 0</a>';
        div += '        </div>';
        div += '    </div>';


        div += '  <div class="row" style="padding: 0 21px;">';
        div += '    <h6>Shared by <strong><a href="#">'+ article_data[i]['posted_by'] +'</a></strong></h6>';
        div += '  </div>';
        div += '</div>';



        div += '<div id="m-card{{forloop.counter0}}" class="card visible-xs {% if forloop.counter0 > 4 %}hide-card{% endif %}">';
        div += '    <div class="row row-eq-height" style="padding: 1em 0.5em;">';
        div += '      <div class="col-xs-12 col-sm-12 card-img" style="position:relative;">';
        div += '          <div style="position:absolute;top: 20px; right: 10px;">';
        div += '            <a href="#" id="bookmark_article_{{article_detail.id}}" rel="{{article_detail.id}}" style="position:absolute;right:15px;"><img id="bookmark-img-{{article_detail.id}}" src="" width="25" alt="" /></a>';
        div += '          </div>';
        div += '          <a href="/website/articles/'+ article_data[i]['id'] +'"><img src="http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com'+ article_data[i]['image'] +'" class="img-responsive" height="239" /></a>';
        div += '      </div>';
        div += '      <div class="col-xs-12 col-sm-12 col-md-8">';
        div += '        <h4 class="card-title"><a href="/website/articles/'+ article_data[i]['id'] +'">'+ article_data[i]['name'] +'</a></h4>';
        div += '        <p class="card-text" align="center">';
        div += '          Shared by <a href="#">'+ article_data[i]['posted_by'] +'</a>';
        div += '        </p>';
        div += '      </div>';
              // <div class="col-xs-12 col-sm-12 visible-xs visible-sm" style="margin-top:15px;">
              //   {% comment %}
              //   <a href="#" class="card-link"><img src="{% static 'website/images/icons/like.png' %}" width="20" /> {{article_detail.like}}</a>
              //   <a href="#" class="card-link"><img src="{% static 'website/images/icons/share.png' %}" width="20" /> 10</a>
              //   <a href="#" class="card-link"><img src="{% static 'website/images/icons/comments.png' %}" width="20" /> {{article_detail.comment}}</a>
              //   {% endcomment %}
              // </div>
        div += '  </div>';
        div += '  <div class="row" style="background:#f9a533;color:white;margin:0px;">';
        div += '    <div class="col-md-12 col-sm-12 col-xs-12"><center>'+ article_data[i]['source'] +'</center></div>';
        div += '  </div>';

        div += '</div>';
        $('#my_articles').append(div);
      });

      }
    },
    error: function(err) {
      $('#my_articles').html('<span style="color:red;"><strong>Articles cannot be loaded!</strong></span>');
    }
  });
}

function my_followers(page_number) {
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/myprofile_followers/?follow=followers&page=' + page_number,
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("SESSION-ID", session_id);
    },
    success: function(data) {
      console.log(data)

      if (!data['data']) {
        var li = '';
        li += '<span style="color:darkgrey;padding:10px;"><strong>You have no followers.</strong></span>'
        $('#disp_followers').append(li);
      } else {
        $(data['data']).each(function(i) {
          var li = '';
          li += '<li><div style="display:inline-block;padding:10px;">';
          li += '    <div class="flex-container">';
          li += '      <div class="flex-item">';
          li += '        <div class="thumbnail" style="border-radius:50%;width:50px;height:50px;margin:0;padding:0;"><a href="http://localhost:8000/website/user/' + data['data'][i]['followed_by_user_id'] + '/"><img src="http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com' + data['data'][i]['image'] + '" /></a></div>';
          li += '      </div>';
          li += '      <div class="flex-item">';
          li += '        <a href="http://localhost:8000/website/user/' + data['data'][i]['followed_by_user_id'] + '/"><h5 style="color:black;font-weight:bold;">' + data['data'][i]['followed_by'] + '</h5></a>';
          li += '      </div>';
          li += '    </div>';
          //li += '  <div style="padding-left: 10px;margin-top: 10px;"><button type="button" class="btn-follow" name="button">Unfollow</button></div></div></li>';
          $('#disp_followers').append(li);
        });
      }
    }
  });
}

function my_following(page_number) {
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/myprofile_followers/?follow=following&page=' + page_number,
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("SESSION-ID", session_id);
    },
    success: function(data) {
      console.log(data['error']);

      if(data['error']) {
        var li = '';
        li += '<span style="color:darkgrey;padding:10px;"><strong>You are following no one.</strong></span>'
        $('#disp_following').append(li);
      }
      else {
      $(data['data']).each(function(i) {
        var li = '';
        console.log(data['data'][i] + ' = ' + i);
        li += '<li><div style="display:inline-block;padding:10px;">';
        li += '    <div class="flex-container">';
        li += '      <div class="flex-item">';
        li += '        <div class="thumbnail" style="border-radius:50%;width:50px;height:50px;margin:0;padding:0;"><a href="http://localhost:8000/website/user/' + data['data'][i]['follower_user_id'] + '/"><img src="http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com' + data['data'][i]['image'] + '" /></a></div>';
        li += '      </div>';
        li += '      <div class="flex-item">';
        li += '        <center><a href="http://localhost:8000/website/user/' + data['data'][i]['follower_user_id'] + '/"><h5 style="color:black;font-weight:bold;">' + data['data'][i]['follow'] + '</h5></a></center>';
        li += '      </div>';
        li += '    </div>';
        //li += '  <div style="padding-left: 10px;margin-top: 10px;"><button type="button" class="btn-follow" id="unfollow-btn-' + data['data'][i]['follower_user_id'] + '" name="button">Unfollow</button></div></div></li>';
        $('#disp_following').append(li);
      });
      }

      $('button[id^=unfollow-btn-]').on('click', function(e) {
        console.log(e['currentTarget']['id']);
        $.post('http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/unfollow_user/', {SESSION_ID:session_id, unfollow:e['currentTarget']['id'].split("-")[2]}).done(function(data) {
          console.log(data)
          $('#disp_following').html('')
          my_following(1)
        });
      })
    }
  });
}

function my_events() {
  return $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/myprofile_events/?filter=latest',
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader("SESSION-ID", session_id);
    },
    success: function(data) {
      console.log('My Events :');
      console.log(data);

      if (data['status'] == 0)
        $('#my_events').html('<strong>' + data['error'] + '</strong>')
      else {

        $.each(data['data'], function(i, val) {
          article_data = data['data'];

          tags_arr = article_data[i]['tags'].split(' ');

          var div = '';
          div += '<div id="card{{forloop.counter0}}" class="card visible-sm visible-md visible-lg {% if forloop.counter0 > 4 %}hide-card{% endif %}" style="width:100%;margin:0 auto;margin-bottom:10px;">';
          div += '    <div class="row row-eq-height" style="padding: 1em 0.5em;">';
          div += '        <div class="col-xs-12 col-sm-12 col-md-3">';
          div += '            <a href="/website/events/'+ article_data[i]['id'] +'"><div class="img" style="background-image:url(\'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com'+ article_data[i]['image'] +'\')"></div></a>';
          div += '        </div>';
          div += '        <div class="col-xs-12 col-sm-12 col-md-9">';
          div += '          <h4 class="card-title">';
          div += '            <a href="/website/events/'+ article_data[i]['id'] +'">'+ article_data[i]['name'] +'</a>';
          div += '            <a href="#" id="bookmark_article_{{article_detail.id}}" rel="{{article_detail.id}}" style="position:absolute;right:15px;"><img id="bookmark-img-{{article_detail.id}}" src="" width="25" alt="" /></a>';
          div += '          </h4>';
          div += '          <p class="card-text">';
          div += '            <span>'+ article_data[i]['source'] +'</span>';
          div += '            <div class="tag-list">';
          if (tags_arr.length > 1) {
            console.log('Tags length : ' + tags_arr.length);
            div += '              <ul class="list-inline">';
            $.each(tags_arr, function(i, val) {
              div += '<li><a href="#">'+ val +'</a></li>'
            });
            div += '              </ul>';
          }
          //div += '                  <li><a href="#">{{tag}}</a></li>';
          div += '            </div></p>';
          div += '          <div class="hidden-xs hidden-sm" style="position:absolute;bottom:0;">';
          div += '                              <a href="#" class="card-link" id="like_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/like.png" %}" width="20"  /> 0</a>';
          div += '              <a href="#" class="card-link"><img src="/static/website/images/icons/share.png"  width="20" /> 10</a>';
          div += '                              <a href="#" class="card-link" id="comment_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/comments.png" width="20"  /> 0</a>';

          div += '          </div>';
          div += '        </div>';
          div += '        <div class="col-xs-12 col-sm-12 visible-sm" style="margin-top:15px;">';
          div += '          <a href="#" class="card-link" id="like_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/like.png" width="20" /> 0</a>';
          div += '          <a href="#" class="card-link"><img src="/static/website/images/icons/share.png" width="20" /> 10</a>';
          div += '          <a href="#" class="card-link" id="m_comment_{{article_detail.id}}" rel="{{article_detail.id}}"><img src="/static/website/images/icons/comments.png" width="20" /> 0</a>';
          div += '        </div>';
          div += '    </div>';


          div += '  <div class="row" style="padding: 0 21px;">';
          div += '    <h6>Shared by <strong><a href="#">'+ article_data[i]['posted_by'] +'</a></strong></h6>';
          div += '  </div>';
          div += '</div>';



          div += '<div id="m-card{{forloop.counter0}}" class="card visible-xs {% if forloop.counter0 > 4 %}hide-card{% endif %}">';
          div += '    <div class="row row-eq-height" style="padding: 1em 0.5em;">';
          div += '      <div class="col-xs-12 col-sm-12 card-img" style="position:relative;">';
          div += '          <div style="position:absolute;top: 20px; right: 10px;">';
          div += '            <a href="#" id="bookmark_article_{{article_detail.id}}" rel="{{article_detail.id}}" style="position:absolute;right:15px;"><img id="bookmark-img-{{article_detail.id}}" src="" width="25" alt="" /></a>';
          div += '          </div>';
          div += '          <a href="/website/events/'+ article_data[i]['id'] +'"><img src="http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com'+ article_data[i]['image'] +'" class="img-responsive" height="239" /></a>';
          div += '      </div>';
          div += '      <div class="col-xs-12 col-sm-12 col-md-8">';
          div += '        <h4 class="card-title"><a href="/website/articles/'+ article_data[i]['id'] +'">'+ article_data[i]['name'] +'</a></h4>';
          div += '        <p class="card-text" align="center">';
          div += '          Shared by <a href="#">'+ article_data[i]['posted_by'] +'</a>';
          div += '        </p>';
          div += '      </div>';
          div += '  </div>';
          div += '  <div class="row" style="background:#f9a533;color:white;margin:0px;">';
          div += '    <div class="col-md-12 col-sm-12 col-xs-12"><center>'+ article_data[i]['source'] +'</center></div>';
          div += '  </div>';

          div += '</div>';
          $('#my_events').append(div);
        });

      }
    }
  });
}

/*function change_status() {
  return $.ajax({
    url: ''
  });
}*/

$(window).on('load', function() {
  var profile_detail = my_profile_detail();
  var profile_status = my_profile_status();
  my_articles();
  my_followers(1);
  my_following(1);
  my_events();
  var profile_data = {};
  $.when(profile_detail, profile_status).done(
    function(data, data2) {
      profile_data = data[0]['data'];
      profile_status = data2[0]['data'];

      var src = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com'+profile_data['profile_image'];
      $('#profile_user_name').html(profile_data['user']);
      $('#profile_picture').attr('src', src);
      console.log(profile_status['status']);
      $('#status-text').html('"'+profile_status['status']+'"');
      $('#score').html(profile_data['score']);
      $('#profile_brief').html(profile_data['about']);
      $('#college').html(profile_data['college']);
      $('#designation').html(profile_data['designation']);
      $('#working-at').html(profile_data['working_at']);

      $('#new-college').attr('value', $('#college').text())
      $('#new-working-at').attr('value', $('#working-at').text())
      $('#new-designation').attr('value', $('#designation').text())

      console.log('Profile Brief : ' + $('#profile_brief').text())
      $('#new-profile-brief').html($('#profile_brief').text())

      $('.profile-list').on('click', function() {
        $(this).css('display', 'none');
        $('#edit-profile').css('display', 'block');
      })

      // cancel-btn for work, college, designation details
      $('#cancel-btn').on('click', function() {
        if ($('#edit-profile').css('display') == 'block' && $('.profile-list').css('display') == 'none') {
          $('#edit-profile').css('display', 'none');
          $('.profile-list').css('display', '');
        }
      })


      // Save work, college, designation details
      $('#save-btn').on('click', function() {

      })

      // Change status

      // Change about
      $('#profile_brief').on('mouseover', function() {
        $(this).css('background', 'lightgrey');
      })
      $('#profile_brief').on('mouseout', function() {
        $(this).css('background', 'white');
      })
      $('#profile_brief').on('click', function() {
        $(this).css('display', 'none');
        $('#edit-profile-brief').css('display', 'block');
      })
      $('#cancel-brief').on('click', function() {
        if ($('#edit-profile-brief').css('display') == 'block' && $('#profile_brief').css('display') == 'none') {
          $('#edit-profile-brief').css('display', 'none');
          $('#profile_brief').css('display', 'block');
        }
      })
      $('#save-brief').on('click', function() {
        //if ($('#new-profile-brief').val())
        $.ajax({
          url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/fill_userprofile/',
          type: 'POST',
          data: {
            SESSION_ID: Cookies.get('user_session_cookie'),
            about: $('#new-profile-brief').val()
          }
        }).done(function() {
          $('#profile_brief').html($('#new-profile-brief').val());
          if ($('#edit-profile-brief').css('display') == 'block' && $('#profile_brief').css('display') == 'none') {
            $('#edit-profile-brief').css('display', 'none');
            $('#profile_brief').css('display', 'block');
          }
        });

      })

      $('.thumbnail').on('mouseover', function() {
        $('#edit-image').css('display', 'block');
      })
      $('.thumbnail').on('mouseout', function() {
        $('#edit-image').css('display', 'none');
      })


    }
  );



});
