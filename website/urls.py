from django.conf.urls import url, include
from django.views.generic import TemplateView
from website import views
urlpatterns = [
	#url(r'^$', TemplateView.as_view(template_name="website/article/articleList.html"), name='index'),
    url(r'^$', views.IndexView.as_view()),
    url(r'^contributor/$',views.ContributorView.as_view(),name='contributor'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^register/', views.RegisterView.as_view(), name='register'),
    url(r'^logout/', views.LogoutView.as_view(), name='logout'),
    url(r'^articles/$', views.ArticleView.as_view(), name='articles'),
    url(r'^articles/(?P<article_id>\d+)/$', views.ArticleDetail.as_view()),
    url(r'^events/$', views.EventList.as_view(), name='events'),
    url(r'^events/(?P<event_id>\d+)/$', views.EventDetail.as_view(), name='event_detail'),
    url(r'^profile/', views.MyProfileView.as_view(), name='my_profile'),
    url(r'^user/(?P<user_id>\d+)/$', views.OtherUserProfileView.as_view()),
    url(r'^bookmarks/$', views.BookmarksView.as_view(), name='bookmarks'),
    url(r'^post_article/$', views.PostArticle.as_view()),
    url(r'^post_event/$', views.PostEvent.as_view()),
    url(r'^settings/$', views.Settings.as_view(), name='settings'),
]
