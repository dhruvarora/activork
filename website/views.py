from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.views import View
import urllib2, json, requests
from website.forms import UserLoginForm
from django.contrib.auth import authenticate, login
from socket import gethostname, gethostbyname
import socket

# Create your views here.

logged_in = False;

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class LoginView(View):
    template_name = 'website/auth/login.html'

    def get(self, request):
        if request.session.get('user_session', None):
            return redirect('/website/articles')
        else:
            return render(request, self.template_name)

    def post(self, request):
        request.session['device_id'] = get_client_ip(request)
        request.session['user_session'] = request.COOKIES['user_session_cookie']
        request.session['email'] = request.COOKIES['email']
        request.session['first_name'] = request.COOKIES['first_name']
        request.session['last_name'] = request.COOKIES['last_name']
        request.session['phone'] = request.COOKIES['phone']
        request.session['username'] = request.COOKIES['username']
	request.session["profile_image"] = request.COOKIES.get("profile_image","/media/profile_photos/defaultuser.jpg")
        print request.session['user_session']
        return redirect('/website/articles')

class RegisterView(View):
    template_name = 'website/auth/register.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self,request):
	email = request.POST["email"]
	password = request.POST["password"]
	login_url = "http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/login/"
	response = requests.post(login_url,data={"email":email,"password":password,"source":"web"})
	response = response.json()
	request.session['device_id'] = get_client_ip(request)
        request.session['user_session'] = response['SESSION_ID']
        request.session['email'] = response["user_data"]['email']
        request.session['first_name'] = response["user_data"]['first_name']
        request.session['last_name'] = response["user_data"]['last_name']
        request.session['phone'] = response["user_data"]['phone_no']
        request.session['username'] = response["user_data"]['username']
	request.session['profile_image'] = response.get("profile_image","/media/profile_photos/defaultuser.jpg")
        print request.session['user_session']
	"""request.session['device_id'] = get_client_ip(request)
        request.session['user_session'] = request.COOKIES['user_session_cookie']
        request.session['email'] = request.COOKIES['email']
        request.session['first_name'] = request.COOKIES['first_name']
        request.session['last_name'] = request.COOKIES['last_name']
        request.session['phone'] = request.COOKIES['phone']
        request.session['username'] = request.COOKIES['username']
	request.session["profile_image"] = request.COOKIES.get("profile_image","/media/profile_photos/defaultuser.jpg")"""

        print request.session['user_session']
        return redirect('/website/articles')


class LogoutView(View):
    def get(self, request):
        if request.session.get('user_session'):
            del request.session['user_session']
            return redirect('/website')
        else:
            return redirect('/website')

class IndexView(View):
    template_name = 'website/index.html'

    def get(self, request):
        return redirect('/website/articles/')
        # url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/interest_names/'
        # json_obj = urllib2.urlopen(url)
        # data = json.load(json_obj)
        # print data['interest']
        # context = {
        #     'interests': data['interest']
        # }
        #
        # return render(request, self.template_name, context)


class ArticleView(View):
    template_name = 'website/article/article_list.html'

    def get(self, request):

        try:
            dev_id = socket.getfqdn()
        except:
            try:
                	dev_id = request.session["device_id"]
            except:
                try:
                    dev_id = request.COOKIES["device_id"]
                except:
                    dev_id = get_client_ip(request)


    	url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/earlier_home/?device_id='+dev_id+'&attr=article'

        #dev_id = request.COOKIES["device_id"]
        #

        # dev_id = gethostbyname(gethostname())
        # print dev_id

        reg_dev_url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/get_details/'
        r = requests.post(reg_dev_url, data = {'device_id':dev_id, 'latitude':'1.920', 'longitude':'2.111'})

        url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/earlier_home/?device_id='+dev_id+'&attr=article'

        print url
        json_obj = urllib2.urlopen(url)
	#return HttpResponse(json_obj)
        data = json.load(json_obj)

        #print request.COOKIES['device_id']

        article_list = data['all_data']
        print article_list
        context = {
            'article_list': article_list,
            'logged_in': logged_in,
	    'device_id':dev_id
        }

        return render(request, self.template_name, context)

class ArticleDetail(View):
    template_name = 'website/article/article_detail.html'

    def get(self, request, *args, **kwargs):
	request.COOKIES['device_id'] = get_client_ip(request)
	dev_id = request.COOKIES['device_id']
        url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/article_event_data/?category=article&id=' + self.kwargs['article_id']
        json_obj = urllib2.urlopen(url)
        data = json.load(json_obj)

	request.COOKIES['selected-interest'] = data['data']["primary_interest"]["interest"]

        article_detail = data['data']
        similar_article_detail = article_detail['similar']
        tags = article_detail['tags'].split(',')
        print article_detail['action']

        query = article_detail['action'].replace(' ', '+')
        # with radius
        #https://maps.googleapis.com/maps/api/place/textsearch/json?query=123+main+street&location=42.3675294,-71.186966&radius=10000&key=
        url2 = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query='+ query +'&key=AIzaSyAnrI-iBcXf2zpXEr2sCDGom-tTDO5GACg'
        json_obj2 = urllib2.urlopen(url2)
        data2 = json.load(json_obj2)

        # print 'Data 2 : '
        # print data2['results']

        context = {
            'article_detail': article_detail,
            'similar_article_detail': similar_article_detail,
            'tags': tags,
            'article_id': self.kwargs['article_id'],
            'places': data2['results'],
	    'device_id':dev_id
        }

        return render(request, self.template_name, context)


class ContributorView(View):
	template_name = 'website/profile/contributor_new.html'
	def get(self,request):
		url = "http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/contributor_page/?detail="
		user_id = request.GET.get("user_detail","activork-1")
		response = requests.get(url+str(user_id))
		json = response.json()
		#return HttpResponse(json.values())
		return render(request,self.template_name,{"context":json})


class EventList(View):
    template_name = 'website/events/event_list.html'

    def get(self, request):
	request.COOKIES['device_id'] = get_client_ip(request)
	dev_id = request.COOKIES['device_id']

        url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/earlier_home/?device_id='+dev_id+'&attr=event'
        json_obj = urllib2.urlopen(url)
        data = json.load(json_obj)

        event_list = data['all_data']
        print event_list

        context = {
            'event_list': event_list,
	    'device_id':dev_id
        }

        return render(request, self.template_name, context)

class EventDetail(View):

    template_name = 'website/events/event_detail.html'

    def get(self, request, *args, **kwargs):
        event_id = self.kwargs['event_id']
        url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/article_event_data/?category=event&id=' + event_id
        json_obj = urllib2.urlopen(url)
        data = json.load(json_obj)

        event_detail = data['data'];
        print event_detail
        #Date, time
        date, time = event_detail['datetime'].split('T')

        context = {
            'event_detail': event_detail,
            'date': date,
            'time': time,
	    'device_id' : get_client_ip(request)
        }

        return render(request, self.template_name, context)

class MyProfileView(View):

    template_name = 'website/profile/my_profile.html'

    def get(self, request):

        if request.session['user_session']:
            return render(request, self.template_name)
        else:
            return redirect('/website/login')

class OtherUserProfileView(View):

    template_name = 'website/profile/other_profile.html'

    def get(self, request, *args, **kwargs):
        user_id = self.kwargs['user_id']
        context = {
            'user_id': user_id
        }
        return render(request, self.template_name, context)

class BookmarksView(View):

    template_name = 'website/bookmarks/my_bookmarks.html'

    def get(self, request, *args, **kwargs):

        if request.session['user_session']:
            return render(request, self.template_name)
        else:
            return redirect('/website/login')

class PostArticle(View):

    template_name = 'website/article/post_article.html'

    def get(self, request):

        url = 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/interest_names/'
        json_obj = urllib2.urlopen(url)
        data = json.load(json_obj)
        print data['interest']
        context = {
            'interests': data['interest']
        }

        return render(request, self.template_name, context)

	session_id = request.GET.get("session_id")
        return render(request, self.template_name,{"session_id":session_id})


    def post(self,request):
	url = request.POST['url']
	code = request.POST['code']
	session_id = request.POST["session_id"]
	interests = request.POST.getlist('interests')
	server_url = "http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/scrap/"
	response = requests.post(server_url,data={'url':url,'code':code,"SESSION_ID":session_id,"interests":interests})

	json = response.json()
	return HttpResponse(json.values())



class PostEvent(View):

    template_name = 'website/events/post_event.html'

    def get(self, request):
        return render(request, self.template_name)

class Settings(View):

    template_name = 'website/settings.html'

    def get(self, request):
        return render(request, self.template_name)
