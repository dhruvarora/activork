$(window).on('load', function() {
  var user_session = Cookies.get('user_session_cookie');
  console.log(user_session);
  var path = window.location.pathname;
  var article_id = path.split("/");
  console.log(article_id[3]);

  // https://maps.googleapis.com/maps/api/place/textsearch/output?parameters

  

  //List comments

  $.ajax({
    url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/comment_on_article/?article_id='+article_id[3],
    type: 'GET',
    headers: {
      'SESSION-ID': user_session
    }
  }).done(function(data) {
    var comments = data['comments']
    var li = '';

    if (comments == undefined) {
      $('#no-comment').removeAttr('style')
      console.log('asd')
    }

    $(comments).each(function(i) {
      li = '';
      li += '<li>';
      li += '    <div class="row">';
      li += '        <div class="col-xs-12 col-sm-12 col-md-12">';
      li += '            <a href="#">'+ comments[i]['commented_by'] +'</a>';
      li += '        </div>';
      li += '    </div>';
      li += '    <div class="row">';
      li += '        <div class="col-xs-12 col-sm-12 col-md-12">'+ comments[i]['comment'] +'</div>';
      li += '    </div>';
      li += '    <div class="row">';
      li += '        <div class="col-md-12">';
      li += '            <span class="pull-right"><a href="#" class="reply-btn">Reply</a></span>';
      li += '        </div>';
      li += '    </div>';
      li += '</li>';

      $('#comment-list').append(li);
    })
  })

  $('#comments')

  $('#like').on('click', function() {
    console.log(article_id[3]);
    $.ajax({
      url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/like_article/',
      data: {SESSION_ID:user_session, article_id:article_id[3]},
      type: 'POST',
      success: function(data) {
        console.log(data);
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
  $('#bookmark').on('click', function(event) {
    console.log(event);
    $.ajax({
      url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/bookmark/',
      data: {SESSION_ID:user_session, type:"article", type_id:article_id[3]},
      type: 'POST',
      success: function(data) {
        console.log(data);
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
  $('#comment-form').on('submit', function() {
    event.preventDefault();
    var _comment = $('#commentBox').val();
    if (_comment == '' || _comment == null) {
      return false;
    } else {
      $.ajax({
        url: 'http://ec2-52-43-13-235.us-west-2.compute.amazonaws.com/mobile/comment_on_article/',
        data: {SESSION_ID:user_session, article_id:article_id[3], comment:_comment},
        type: 'POST',
        success: function(data) {
          console.log(data);
        },
        error: function(error) {
          console.log(error);
        }
      });
    }

    console.log(_comment);
  })
});
